package library.tests;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.IBook;
import library.entities.ICalendar;
import library.entities.ILibrary;
import library.entities.ILoan;
import library.entities.IPatron;
import library.entities.helpers.BookHelper;
import library.entities.helpers.CalendarFileHelper;
import library.entities.helpers.LibraryFileHelper;
import library.entities.helpers.LoanHelper;
import library.entities.helpers.PatronHelper;

class LibraryTest {
	

	IPatron patron;
	IBook book;
	ILoan loan;
	ILibrary library;
	ICalendar calendar;
	PatronHelper patronHelper;
	BookHelper bookHelper;
	LoanHelper loanHelper;
	LibraryFileHelper libraryHelper;
	CalendarFileHelper calendarHelper;
	
	String last;
	String first;
	String email;
	long ph;
	int pId;

	String author;
	String title;
	String callNo;
	int bId;


	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		//arrange
				last = "Parr";
				first = "Matt";
				email = "Matt@";
				ph = 0412456354L;
				pId = 1;

				author = "Matthew Parr";
				title = "Magical me";
				callNo = "100";
				bId = 1;
				
				
				//act
				calendarHelper = new CalendarFileHelper();
				calendar = calendarHelper.loadCalendar();
				libraryHelper = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
				library = libraryHelper.loadLibrary();				
				book = library.addBook(author, title, callNo);
				patron = library.addPatron(last, first, email, ph);
				loan = library.issueLoan(book, patron);
				library.commitLoan(loan);
	}

	@Test
	void testFineIncurred() {
		
		//arrange
		//act
		calendar.incrementDate(3);
		loan.updateOverDueStatus(calendar.getDate());
		library.dischargeLoan(loan, false);

		
		
		//assert
		assertTrue(patron.getFinesPayable() == 1.0);
	}
	
	@Test
	void testGetDueDate() {
		//arrange
		Date date = calendar.getDueDate(2);
		//act
		calendar.incrementDate(3);
		
		//assert
		assertTrue(calendar.getDaysDifference(date) == 1L);
	}
	
	@Test
	void testFineIncurredFixed() {
		//arrange
		//act
		calendar.incrementDate(4);
		loan.updateOverDueStatus(calendar.getDate());
		library.dischargeLoan(loan, false);

		
		
		//assert
		assertTrue(patron.getFinesPayable() == 2.0);
	}

}
